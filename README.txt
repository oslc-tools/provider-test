Overview:

The goal of this README is to provide a quick overview of the OSLC provider tests 
and how to easily use / modify them to validate an OSLC service provider.

The current work focused on OSLC-CM 1.0 and 2.0 implementations.  It can test 
both 1.0 and 2.0 implementations at the same time.  It is intended to work
with any OSLC Core-based implementations such as OSLC-QM and OSLC-RM 2.0.

--------------------------------------------------------------------------------
Running the tests:

Use an existing launch config created for known tests:
  - OSLC V2 RTC.launch
  - OSLC V2 CQ.launch
  
From Eclipse, Run -> Run Configurations... -> JUnits

--------------------------------------------------------------------------------
Provider specific setup:

- Note: for some configurations that use internal IBM hosted servers, you will
  first need to BSO-login before accessing the servers.
- Need to update config/*/*setup*.properties
  - Initial catalog URL
  - Template files for creation tests: config/cq/*template*
  - OAuth registration keys
  
--------------------------------------------------------------------------------
Code Structure:

The DynamicSuiteBuilder class is the driver of the suite, it determines which 
version of OSLC the provider supports and runs the appropriate set of tests 
against it. It does this by simply building an ArrayList of test classes to be 
run and returning it in the suite() method. Future test classes can be quickly 
added by adding them to the ArrayList for the appropriate version of OSLC that 
the class is testing.

When the DynamicSuiteBuilder and the various test classes are running, they pull 
the information about the OSLC service provider from the setup.properties file. 
The other properties files contained in the project are basically guidelines / 
templates to be swapped with the setup.properties file before running. The 
properties file itself contains comments mentioning what each field is used for.

Another thing to note is that right now, the OSLCv2 test suite has two test 
classes for query tests. The one the DynamicSuiteBuilder uses currently is the 
SimplifiedQueryTests, which just checks the basic format of returned query 
results but doesn�t verify that certain results are present and correct. Once 
OSLC Shapes are more fully implemented and we can programmatically create 
records for all the QueryBase elements using the respective Shapes, the more 
comprehensive QueryTests class should probably be used instead. Also, the 
ChangeRequestTests in OSLCv2 requires that the creation factory have a record to
query on to actually retrieve a change request, this class as well as the 
CreationAndUpdateTests class can both be improved by utilizing shapes to 
programmatically create records on the fly.

oauth_callback=&authorize=true&oauth_token=ffb50d60ef6f4b209fddf182a87c2877