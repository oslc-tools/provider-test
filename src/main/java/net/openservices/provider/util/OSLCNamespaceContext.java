/*******************************************************************************
 * Copyright IBM Corporation 2010.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package net.openservices.provider.util;

import java.util.Iterator;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;


public class OSLCNamespaceContext implements NamespaceContext {
	public String getNamespaceURI(String prefix) {
		if (prefix == null) throw new NullPointerException("Null prefix");
        else if ("jfs".equals(prefix)) return OSLCConstants.JFS;
        else if ("rdfs".equals(prefix)) return OSLCConstants.RDFS;
        else if ("rdf".equals(prefix)) return OSLCConstants.RDF;
        else if ("dc".equals(prefix)) return OSLCConstants.DC;
        else if ("jd".equals(prefix)) return OSLCConstants.JD;
        else if ("jp06".equals(prefix)) return OSLCConstants.JP06;
        else if ("oslc_disc".equals(prefix)) return OSLCConstants.OSLC_DISC;
        else if ("oslc".equals(prefix)) return OSLCConstants.OSLC_V2;
        else if ("oslc_core".equals(prefix)) return OSLCConstants.OSLC_V2;
        else if ("oslc_v2".equals(prefix)) return OSLCConstants.OSLC_V2;
        else if ("oslc_cm".equals(prefix)) return OSLCConstants.OSLC_CM;
        else if ("rtc_cm".equals(prefix)) return OSLCConstants.RTC_CM;
        else if ("atom".equals(prefix)) return OSLCConstants.ATOM;
        else if ("oslc_cm_v2".equals(prefix)) return OSLCConstants.OSLC_CM_V2;
        return XMLConstants.NULL_NS_URI;
	}
    public String getPrefix(String uri) {return null;}

    @SuppressWarnings({ "unchecked" })
	public Iterator getPrefixes(String uri) {return null;}
}
