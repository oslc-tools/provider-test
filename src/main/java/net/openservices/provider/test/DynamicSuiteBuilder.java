/*******************************************************************************
 * Copyright IBM Corporation 2010.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package net.openservices.provider.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import net.openservices.provider.test.oslcv1tests.CreationAndUpdateTests;
import net.openservices.provider.test.oslcv1tests.OAuthTests;
import net.openservices.provider.test.oslcv1tests.QueryTests;
import net.openservices.provider.test.oslcv1tests.ServiceDescriptionTests;
import net.openservices.provider.test.oslcv1tests.ServiceProviderCatalogTests;
import net.openservices.provider.test.oslcv2tests.TestsBase;
import net.openservices.provider.util.OSLCConstants;
import net.openservices.provider.util.SetupProperties;

import org.junit.runner.RunWith;


/**
 * This class determines which version of OSLC the provider is using,
 * and builds the correct set of OSLC Provider tests accordingly.
 * 
 * Also sets up forms authentication if necessary (for things like RTC).
 * 
 */
@RunWith(OslcTestSuite.class)
public class DynamicSuiteBuilder
{
	public static Class<?>[] suite() throws IOException
	{
		Properties setupProps = SetupProperties.setup(null);
		
		TestsBase.staticSetup();
		
		//If we also want to run v1 tests (assuming this is a v2 provider)
		String testVersions = setupProps.getProperty("testVersions");

		//Determine if this is a v1 or v2 provider
		ArrayList<Class<?>> testsToRun = new ArrayList<Class<?>>();
		
		if (OSLCConstants.OSLC_CM_V2.equals(testVersions) || OSLCConstants.OSLC_V2.equals(testVersions) || testVersions.equals("both"))
		{
			testsToRun.add(net.openservices.provider.test.oslcv2tests.ServiceProviderCatalogXmlTests.class);
			testsToRun.add(net.openservices.provider.test.oslcv2tests.ServiceProviderCatalogRdfXmlTests.class);
			testsToRun.add(net.openservices.provider.test.oslcv2tests.ServiceProviderXmlTests.class);
			testsToRun.add(net.openservices.provider.test.oslcv2tests.ServiceProviderRdfXmlTests.class);
			testsToRun.add(net.openservices.provider.test.oslcv2tests.CreationAndUpdateXmlTests.class);
			testsToRun.add(net.openservices.provider.test.oslcv2tests.CreationAndUpdateRdfXmlTests.class);
			testsToRun.add(net.openservices.provider.test.oslcv2tests.CreationAndUpdateJsonTests.class);
			testsToRun.add(net.openservices.provider.test.oslcv2tests.ChangeRequestXmlTests.class);
			testsToRun.add(net.openservices.provider.test.oslcv2tests.ChangeRequestRdfXmlTests.class);
			testsToRun.add(net.openservices.provider.test.oslcv2tests.SimplifiedQueryXmlTests.class);
			testsToRun.add(net.openservices.provider.test.oslcv2tests.SimplifiedQueryRdfXmlTests.class);
			//testsToRun.add(net.openservices.provider.test.oslcv2tests.QueryTests.class);
			testsToRun.add(net.openservices.provider.test.oslcv2tests.OAuthTests.class);
		}
		if (OSLCConstants.OSLC_CM.equals(testVersions) || testVersions.equals("both"))
		{
			testsToRun.add(ServiceProviderCatalogTests.class);
			testsToRun.add(ServiceDescriptionTests.class);
			testsToRun.add(CreationAndUpdateTests.class);
			testsToRun.add(QueryTests.class);
			testsToRun.add(OAuthTests.class);
		}
		//Return array of test classes
		return testsToRun.toArray(new Class<?>[0]);
	}
}