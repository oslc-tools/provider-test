/*******************************************************************************
 * Copyright IBM Corporation 2010.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package net.openservices.provider.test;

import java.io.IOException;

import org.junit.runners.Suite;
import org.junit.runners.model.InitializationError;

/**
 * An extension to JUnit's Suite, this class's job
 * is to allow DynamicSuiteBuilder to properly create
 * the test suite for the correct version of OSLC.
 * 
 * @author Matthew Brown
 *
 */
public class OslcTestSuite extends Suite
{
	public OslcTestSuite(Class<?> setupClass) throws InitializationError, IOException
	{
		super(setupClass, DynamicSuiteBuilder.suite());
	}
}
