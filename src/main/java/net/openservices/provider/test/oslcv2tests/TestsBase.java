/*******************************************************************************
 * Copyright IBM Corporation 2010.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package net.openservices.provider.test.oslcv2tests;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathExpressionException;

import net.openservices.provider.util.OSLCConstants;
import net.openservices.provider.util.OSLCUtils;
import net.openservices.provider.util.SetupProperties;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Selector;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;


public class TestsBase {
	public enum AuthMethods {BASIC, FORM, OAUTH};

	protected static Credentials basicCreds;
	protected static boolean onlyOnce = true;
	protected static Properties setupProps = null;
	protected static String xmlCreateTemplate;
	protected static String xmlUpdateTemplate;
	protected static String rdfXmlCreateTemplate;
	protected static String rdfXmlUpdateTemplate;
	protected static String jsonCreateTemplate;
	protected static String jsonUpdateTemplate;
	protected static String updateParams;
	protected static Header[] headers;
	protected static AuthMethods authMethod = AuthMethods.BASIC;

	protected String currentUrl = null;      // URL of current service being tested
	protected static String setupBaseUrl = null;  // Configuration baseUrl, think ServiceProvider or ServiceProviderCatalog
	
	public TestsBase(String thisUrl) {
		currentUrl = thisUrl;
	}
	
	public static void staticSetup() {
		if (setupProps == null) {
			setupProps = SetupProperties.setup(null);
			updateParams = setupProps.getProperty("updateParams");
			String userId = setupProps.getProperty("userId");
			String pw = setupProps.getProperty("pw");
			basicCreds = new UsernamePasswordCredentials(userId, pw);
			Header h = new BasicHeader("OSLC-Core-Version", "2.0");
			headers = new Header[] { h };
			String onlyOnceStr = setupProps.getProperty("runOnlyOnce");
			if (onlyOnceStr != null && onlyOnceStr.equals("false")) {
				onlyOnce = false;
			}
			setupBaseUrl = setupProps.getProperty("baseUri");
			String authType = setupProps.getProperty("authMethod");
			if (authType.equalsIgnoreCase("OAUTH")) {
				authMethod = AuthMethods.OAUTH;
			} else if (authType.equalsIgnoreCase("FORM")) {
				authMethod = AuthMethods.FORM;
				formLogin(userId, pw);
			}

			// First, Setup plain old XML
			String fileName = setupProps.getProperty("createTemplateXmlFile");
			if (fileName != null)
				xmlCreateTemplate = OSLCUtils.readFileByNameAsString(fileName);
			fileName = setupProps.getProperty("updateTemplateXmlFile");
			if (fileName != null)
				xmlUpdateTemplate = OSLCUtils.readFileByNameAsString(fileName);
			// Now RDF/XML
			fileName = setupProps.getProperty("createTemplateRdfXmlFile");
			if (fileName != null)
				rdfXmlCreateTemplate = OSLCUtils.readFileByNameAsString(fileName);
			fileName = setupProps.getProperty("updateTemplateRdfXmlFile");
			if (fileName != null)
				rdfXmlUpdateTemplate = OSLCUtils.readFileByNameAsString(fileName);
			// Now JSON
			fileName = setupProps.getProperty("createTemplateJsonFile");
			if (fileName != null)
				jsonCreateTemplate = OSLCUtils.readFileByNameAsString(fileName);
			fileName = setupProps.getProperty("updateTemplateJsonFile");
			if (fileName != null)
				jsonUpdateTemplate = OSLCUtils.readFileByNameAsString(fileName);
			// Now handle if RDF/XML wasn't given
			if (rdfXmlCreateTemplate == null)
				rdfXmlCreateTemplate = xmlCreateTemplate;
			if (rdfXmlUpdateTemplate == null)
				rdfXmlUpdateTemplate = xmlUpdateTemplate;
		}
	}

	public void setup() throws IOException, ParserConfigurationException,
			SAXException, XPathException {
		staticSetup();
	}

	public static ArrayList<String> getServiceProviderURLsUsingXML(String inBaseURL)
			throws IOException, XPathException, ParserConfigurationException,
			SAXException {
		return getServiceProviderURLsUsingXML(inBaseURL, onlyOnce);
	}
	
	public static Collection<Object[]> toCollection(ArrayList<String> list) {
		Collection<Object[]> data = new ArrayList<Object[]>();
		for (String string : list) {
			data.add(new Object[] {string});
		}
		return data;	
	}
	
	public static ArrayList<String> getServiceProviderURLsUsingXML(String inBaseURL, boolean dontGoDeep)
		throws IOException, XPathException, ParserConfigurationException,
		SAXException {
		staticSetup();
		String base=null;
		if (inBaseURL == null) 
			base = setupBaseUrl;
		else
			base = inBaseURL;
		HttpResponse resp = OSLCUtils.getResponseFromUrl(base, base,
				basicCreds, OSLCConstants.CT_XML, headers);

		Document baseDoc = OSLCUtils.createXMLDocFromResponseBody(EntityUtils
				.toString(resp.getEntity()));

		// ArrayList to contain the urls from all SPCs
		ArrayList<String> data = new ArrayList<String>();

		// Get all ServiceProvider urls from the base document in order to
		// recursively add all the capability urls from them as well.
		NodeList sps = (NodeList) OSLCUtils.getXPath().evaluate(
				"//oslc_v2:ServiceProvider/@rdf:about", baseDoc,
				XPathConstants.NODESET);
		for (int i = 0; i < sps.getLength(); i++) {
			if (!sps.item(i).getNodeValue().equals(base) || sps.getLength() == 1) {
				data.add(sps.item(i).getNodeValue());
				if (dontGoDeep)
					return data;
			}
		}

		// Get all ServiceProviderCatalog urls from the base document in order
		// to recursively add all the capability from ServiceProviders within them as well.
		NodeList spcs = (NodeList) OSLCUtils.getXPath().evaluate(
				"//oslc_v2:ServiceProviderCatalog/@rdf:about", baseDoc,
				XPathConstants.NODESET);
		for (int i = 0; i < spcs.getLength(); i++) {
			if (!spcs.item(i).getNodeValue().equals(base)) {
				ArrayList<String> subCollection = getServiceProviderURLsUsingXML(spcs
						.item(i).getNodeValue(), dontGoDeep);
				for (String subUri : subCollection) {
					data.add(subUri);
					if (dontGoDeep)
						return data;
				}
			}
		}

		return data;
	}
	
	public static ArrayList<Node> getCapabilityDOMNodesUsingXML(String xpathStmt,
			ArrayList<String> serviceUrls) throws IOException,
			ParserConfigurationException, SAXException,
			XPathExpressionException {
		// Collection to contain the creationFactory urls from all SPs
		ArrayList<Node> data = new ArrayList<Node>();
		
		for (String base : serviceUrls) {
			HttpResponse resp = OSLCUtils.getResponseFromUrl(base, base,
					basicCreds, OSLCConstants.CT_XML, headers);

			Document baseDoc = OSLCUtils.createXMLDocFromResponseBody(EntityUtils
					.toString(resp.getEntity()));

			NodeList sDescs = (NodeList) OSLCUtils.getXPath().evaluate(
					xpathStmt,
					baseDoc, XPathConstants.NODESET);
			for (int i = 0; i < sDescs.getLength(); i++) {
				data.add(sDescs.item(i));
				if (onlyOnce)
					return data;
			}
		}
		return data;
	}
	
	public static ArrayList<String> getCapabilityURLsUsingXML(String xpathStmt,
			ArrayList<String> serviceUrls, boolean useDefaultUsage) throws IOException,
			ParserConfigurationException, SAXException,
			XPathExpressionException {
		// Collection to contain the creationFactory urls from all SPs
		ArrayList<String> data = new ArrayList<String>();
		String firstUrl = null;
		
		for (String base : serviceUrls) {
			HttpResponse resp = OSLCUtils.getResponseFromUrl(base, base,
					basicCreds, OSLCConstants.CT_XML, headers);

			Document baseDoc = OSLCUtils.createXMLDocFromResponseBody(EntityUtils
					.toString(resp.getEntity()));

			NodeList sDescs = (NodeList) OSLCUtils.getXPath().evaluate(
					xpathStmt,
					baseDoc, XPathConstants.NODESET);
			String xpathSubStmt = "../../oslc_v2:usage/@rdf:resource";
			for (int i = 0; i < sDescs.getLength(); i++) {
				if (firstUrl == null)
					firstUrl = sDescs.item(i).getNodeValue();
				if (useDefaultUsage) {
					NodeList usages = (NodeList) OSLCUtils.getXPath().evaluate(
							xpathSubStmt,
							sDescs.item(i), XPathConstants.NODESET);
					for (int u=0; u < usages.getLength(); u++) {
						String usageValue = usages.item(u).getNodeValue();
						if (OSLCConstants.USAGE_DEFAULT_URI.equals(usageValue)) {
							data.add(sDescs.item(i).getNodeValue());
							return data;
						}
					}
				} else {
					data.add(sDescs.item(i).getNodeValue());
					if (onlyOnce)
						return data;
				}
			}
		}
		// If we didn't find the default, then just send back the first one we
		// found.
		if (useDefaultUsage && firstUrl != null)
			data.add(firstUrl);
		return data;
	}

	public static ArrayList<String> getServiceProviderURLsUsingRdfXml(String inBaseURL, boolean dontGoDeep)
	throws IOException {
		staticSetup();
		HttpResponse resp = OSLCUtils.getResponseFromUrl(setupBaseUrl, setupBaseUrl, basicCreds, OSLCConstants.CT_RDF, headers);
		
		assertEquals("Did not successfully retrieve ServiceProviders at: "+setupBaseUrl, HttpStatus.SC_OK, resp.getStatusLine().getStatusCode());
		
	    // ArrayList to contain the urls from all SPCs
	    ArrayList<String> data = new ArrayList<String>();

	    // Used to hold RDF from doing service discovery
		Model spModel = ModelFactory.createDefaultModel(); 
		spModel.read(resp.getEntity().getContent(), OSLCConstants.JENA_RDF_XML);
		
		// Get all the "inlined" definitions for Service Providers, namely
		// all subjects whose rdf:type = oslc:ServiceProvider
		Property rdfType = spModel.createProperty(OSLCConstants.RDF_TYPE_PROP);
		Resource spTypeRes = spModel.getResource(OSLCConstants.SERVICE_PROVIDER_TYPE);
		Selector select = new SimpleSelector(null, rdfType, spTypeRes); 
		StmtIterator statements = spModel.listStatements(select);
		// Since resources can have multiple types, iterate over all
		while (statements.hasNext()) {
			Statement st = statements.nextStatement();
			data.add(st.getSubject().getURI());
			if (dontGoDeep)	return data;
		}

		// Chase any ServiceProviderCatalogs, looking for ServiceProviders definitions.
		Property spcPredicate = spModel.createProperty(OSLCConstants.SERVICE_PROVIDER_CATALOG_PROP);
		select = new SimpleSelector(null, spcPredicate, (RDFNode)null); 
		statements = spModel.listStatements(select);
		while (statements.hasNext()) {
			ArrayList<String> results = getServiceProviderURLsUsingRdfXml(statements.nextStatement().getObject().toString(), dontGoDeep);
			data.addAll(results);
			if (dontGoDeep) return data;
		}		
	    
	    return data;		
	}

	public static ArrayList<String> getCapabilityURLsUsingRdfXml(String propertyUri,
			ArrayList<String> serviceUrls, boolean useDefaultUsage) throws IOException {
		// Collection to contain the creationFactory urls from all SPs
		ArrayList<String> data = new ArrayList<String>();
		String firstUrl = null;
		for (String base : serviceUrls) {
			HttpResponse resp = OSLCUtils.getResponseFromUrl(base, base,
					basicCreds, OSLCConstants.CT_RDF, headers);
			
			Model spModel = ModelFactory.createDefaultModel();
			spModel.read(resp.getEntity().getContent(), OSLCConstants.JENA_RDF_XML);

			Property capProp = spModel.createProperty(propertyUri);
			Property usageProp = spModel.createProperty(OSLCConstants.USAGE_PROP);
			Selector select = new SimpleSelector(null, capProp, (RDFNode)null);
			StmtIterator statements = spModel.listStatements(select);
			while (statements.hasNext()) {
				Statement stmt = statements.nextStatement();
				if (firstUrl == null)
					firstUrl = stmt.getObject().toString();
				if (useDefaultUsage) {
					StmtIterator usages = stmt.getSubject().listProperties(usageProp);
					while (usages.hasNext()) {
						Statement usageStmt = usages.nextStatement();
						if (OSLCConstants.USAGE_DEFAULT_URI.equals(usageStmt.getObject().toString())) {
							data.add(stmt.getObject().toString());
							return data;
						}
					}
				} else {
					data.add(stmt.getObject().toString());
					if (onlyOnce) return data;
				}
			}			
		}
		// If no default usage was found, then just return first one
		if (useDefaultUsage && firstUrl != null)
			data.add(firstUrl);
		return data;
	}

	public static boolean formLogin(String userId, String pw) {
		String formUri = setupProps.getProperty("formUri");
		// Get cookies for forms login procedure (ie: get redirected to login
		// page.
		HttpResponse resp;
		try {
			resp = OSLCUtils.getResponseFromUrl(setupBaseUrl, setupBaseUrl, null, "*/*");
			if (resp.getEntity() != null) {
				resp.getEntity().consumeContent();
			}
			// Post info to forms auth page
			OSLCUtils.setupFormsAuth(formUri, userId, pw);
		} catch (ClientProtocolException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public static void printRdfModel(Model model) {
		StmtIterator listStatements = model.listStatements();
		while (listStatements.hasNext()) {
			Statement s = listStatements.nextStatement();
			System.out.println(s.toString());
		}
	}

}
