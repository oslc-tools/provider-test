/*******************************************************************************
 * Copyright IBM Corporation 2010.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package net.openservices.provider.test.oslcv2tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathException;

import net.openservices.provider.util.OSLCUtils;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.xml.sax.SAXException;

/**
 * This class provides JUnit tests for the validation of the OSLCv2 creation and
 * updating of change requests. It uses the template files specified in
 * setup.properties as the entity to be POST or PUT, for creation and updating
 * respectively.
 * 
 * After each test, it attempts to perform a DELETE call on the resource that
 * was presumably created, but this DELETE call is not technically required in
 * the OSLC spec, so the created change request may still exist for some service
 * providers.
 */
@RunWith(Parameterized.class)
public class CreationAndUpdateBaseTests extends TestsBase {

	public CreationAndUpdateBaseTests(String url) {
		super(url);
	}

	@Before
	public void setup() throws IOException, ParserConfigurationException,
			SAXException, XPathException {
		super.setup();
	}

	@Test
	public void createResourceWithInvalidContentType() throws IOException {
		// Issue post request using the provided template and an invalid
		// contentType
		HttpResponse resp = OSLCUtils.postDataToUrl(currentUrl, basicCreds,
				"*/*", "weird/type", xmlCreateTemplate, headers);
		resp.getEntity().consumeContent();
		assertTrue(resp.getStatusLine().getStatusCode() == 415);
	}

	protected void createValidResourceUsingTemplate(String contentType,
			String accept, String content) throws IOException {
		// Issue post request using the provided template
		HttpResponse resp = OSLCUtils.postDataToUrl(currentUrl, basicCreds,
				accept, contentType, content, headers);

		// Assert the response gave a 201 Created
		String responseBody = EntityUtils.toString(resp.getEntity());
		resp.getEntity().consumeContent();
		assertEquals(responseBody, HttpStatus.SC_CREATED, resp.getStatusLine()
				.getStatusCode());
		Header location = resp.getFirstHeader("Location");
		// Assert that we were given a Location header pointing to the resource,
		// which is not a MUST according to oslc v2, but probably should be
		// present
		// none the less.
		assertFalse(location == null);
		// Attempt to clean up after the test by calling delete on the given
		// url,
		// which is not a MUST according to the oslc cm spec
		resp = OSLCUtils.deleteFromUrl(location.getValue(), basicCreds, "*/*");
		if (resp.getEntity() != null) {
			resp.getEntity().consumeContent();
		}
	}

	protected void createResourceWithInvalidContent(String contentType,
			String accept, String content) throws IOException {
		// Issue post request using valid content type but invalid content
		HttpResponse resp = OSLCUtils.postDataToUrl(currentUrl, basicCreds,
				accept, accept, content, headers);
		resp.getEntity().consumeContent();
		// TODO: What is right sc forbidden?
		assertFalse("Expecting error but received OK", HttpStatus.SC_OK == resp
				.getStatusLine().getStatusCode());
	}

	protected void createResourceAndUpdateIt(String contentType, String accept,
			String newContent, String updateContent) throws IOException {
		// Issue post request using the provided template
		HttpResponse resp = OSLCUtils.postDataToUrl(currentUrl, basicCreds,
				accept, contentType, newContent, headers);

		resp.getEntity().consumeContent();
		assertEquals(HttpStatus.SC_CREATED, resp.getStatusLine()
				.getStatusCode());
		Header location = resp.getFirstHeader("Location");
		assertFalse(location == null);

		// Now, go to the url of the new change request and update it.
		// We may need to add something to update URL to match the template
		String updateUrl = location.getValue();
		if (updateParams != null && !updateParams.isEmpty())
			updateUrl = updateUrl + updateParams;
		resp = OSLCUtils.putDataToUrl(updateUrl, basicCreds, accept,
				contentType, updateContent, headers);
		String responseBody = EntityUtils.toString(resp.getEntity());
		if (resp.getEntity() != null)
			resp.getEntity().consumeContent();
		// Assert that a proper PUT resulted in a 200 OK
		assertEquals("HTTP Response body: \n " + responseBody,
				HttpStatus.SC_OK, resp.getStatusLine().getStatusCode());

		// Clean up after the test by attempting to delete the created resource
		if (location != null) {
			resp = OSLCUtils.deleteFromUrl(location.getValue(), basicCreds,
					"*/*");
			if (resp != null && resp.getEntity() != null)
				resp.getEntity().consumeContent();
		}
	}

	protected void updateCreatedResourceWithInvalidContent(String contentType,
			String accept, String content, String invalidContent)
			throws IOException {
		// Issue post request using the provided template
		HttpResponse resp = OSLCUtils.postDataToUrl(currentUrl, basicCreds,
				accept, contentType, content, headers);

		// Assert the response gave a 201 Created
		resp.getEntity().consumeContent();
		assertEquals(HttpStatus.SC_CREATED, resp.getStatusLine()
				.getStatusCode());
		Header location = resp.getFirstHeader("Location");
		// Assert that we were given a Location header pointing to the resource
		assertNotNull(
				"Expected 201-Created to return non-null Location header",
				location);

		// Now, go to the url of the new change request and update it.
		resp = OSLCUtils.putDataToUrl(location.getValue(), basicCreds, accept,
				contentType, invalidContent, headers);
		if (resp.getEntity() != null) {
			resp.getEntity().consumeContent();
		}
		// Assert that an invalid PUT resulted in a 400 BAD REQUEST
		assertEquals(HttpStatus.SC_BAD_REQUEST, resp.getStatusLine()
				.getStatusCode());

		// Clean up after the test by attempting to delete the created resource
		if (location != null)
			resp = OSLCUtils.deleteFromUrl(location.getValue(), basicCreds, "");

		if (resp != null && resp.getEntity() != null)
			resp.getEntity().consumeContent();
	}

	protected void updateCreatedResourceWithBadType(String contentType,
			String accept, String createContent, String updateContent,
			String badType) throws IOException {
		HttpResponse resp = OSLCUtils.postDataToUrl(currentUrl, basicCreds,
				accept, contentType, createContent, headers);

		// Assert the response gave a 201 Created
		resp.getEntity().consumeContent();
		assertEquals(HttpStatus.SC_CREATED, resp.getStatusLine()
				.getStatusCode());
		Header location = resp.getFirstHeader("Location");

		// Assert that we were given a Location header pointing to the resource
		assertFalse("Expected Location header on 201-Created", location == null);

		// Now, go to the url of the new change request and update it.
		resp = OSLCUtils.putDataToUrl(location.getValue(), basicCreds, "*/*",
				badType, updateContent, headers);
		if (resp != null && resp.getEntity() != null)
			resp.getEntity().consumeContent();
		// TODO: SHould this be 409 or 415 or both?
		assertEquals(HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE, resp.getStatusLine()
				.getStatusCode());

		// Clean up after the test by attempting to delete the created resource
		if (location != null)
			resp = OSLCUtils.deleteFromUrl(location.getValue(), basicCreds, "");

		if (resp != null && resp.getEntity() != null)
			resp.getEntity().consumeContent();
	}

}
