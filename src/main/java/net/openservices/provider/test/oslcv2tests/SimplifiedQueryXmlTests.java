/*******************************************************************************
 * Copyright IBM Corporation 2010.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package net.openservices.provider.test.oslcv2tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathExpressionException;

import net.openservices.provider.util.OSLCConstants;
import net.openservices.provider.util.OSLCUtils;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This class provides JUnit tests for the basic validation of query factories
 * as specified in the OSLC version 2 spec. This version of the query tests only
 * tests the basic status code and form of the query responses, as without
 * shapes implemented it is difficult to represent the needed various templates
 * of different change request types and to query for the templates.
 */
@RunWith(Parameterized.class)
public class SimplifiedQueryXmlTests extends SimplifiedQueryBaseTests {

	public SimplifiedQueryXmlTests(String thisUri) {
		super(thisUri);
	}

	@Before
	public void setup() throws IOException, ParserConfigurationException,
			SAXException, XPathException {
		super.setup();
	}

	@Parameters
	public static Collection<Object[]> getAllDescriptionUrls()
			throws IOException, ParserConfigurationException, SAXException,
			XPathException {
		// Checks the ServiceProviderCatalog at the specified baseUrl of the
		// REST service in order to grab all urls
		// to other ServiceProvidersCatalogs contained within it, recursively,
		// in order to find the URLs of all
		// query factories of the REST service.
		String v = "//oslc_v2:QueryCapability/oslc_v2:queryBase/@rdf:resource";
		ArrayList<String> serviceUrls = getServiceProviderURLsUsingXML(null);
		ArrayList<String> capabilityURLsUsingXML = TestsBase
				.getCapabilityURLsUsingXML(v, serviceUrls, true);
		return toCollection(capabilityURLsUsingXML);
	}

	protected void validateNonEmptyResponse(String query)
			throws XPathExpressionException, IOException,
			ParserConfigurationException, SAXException {
		HttpResponse response = OSLCUtils.getResponseFromUrl(setupBaseUrl,
				currentUrl + query, basicCreds, OSLCConstants.CT_XML, headers);
		assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
		String responseBody = EntityUtils.toString(response.getEntity());
		Document doc = OSLCUtils.createXMLDocFromResponseBody(responseBody);
		Node results = (Node) OSLCUtils.getXPath().evaluate(
				"//oslc:ResponseInfo/@rdf:about", doc, XPathConstants.NODE);
		assertNotNull("Expected ResponseInfo", results);
		assertEquals("Expended ResponseInfo/@rdf:about to equal request URL",
				currentUrl + query, results.getNodeValue());
		results = (Node) OSLCUtils.getXPath().evaluate("//oslc:totalCount",
				doc, XPathConstants.NODE);
		assertNotNull("Expected oslc:totalCount", results);
		int totalCount = Integer.parseInt(results.getTextContent());
		assertTrue("Expected oslc:totalCount > 0",
				totalCount > 0);

		NodeList resultList = (NodeList) OSLCUtils.getXPath().evaluate(
				"//rdf:Description/rdfs:member", doc, XPathConstants.NODESET);
		assertNotNull("Expected rdfs:member(s)", resultList);
		assertNotNull("Expected > 1 rdfs:member(s)", resultList.getLength() > 0);
	}

	@Test
	public void validEqualsQueryContainsExpectedResource() throws IOException,
			ParserConfigurationException, SAXException,
			XPathExpressionException {
		String query = getQueryUrlForalidEqualsQueryContainsExpectedResources();
		validateNonEmptyResponse(query);
	}

	@Test
	public void validNotEqualQueryContainsExpectedResource()
			throws IOException, SAXException, ParserConfigurationException,
			XPathExpressionException {
		String query = getQueryUrlForValidNotEqualQueryContainsExpectedResources();
		validateNonEmptyResponse(query);
	}

	@Test
	public void validLessThanQueryContainsExpectedResources()
			throws IOException, SAXException, ParserConfigurationException,
			XPathExpressionException, ParseException {
		String query = getQueryUrlForValidLessThanQueryContainsExpectedResources();
		validateNonEmptyResponse(query);
	}

	@Test
	public void validGreaterThanQueryContainsExpectedDefects()
			throws IOException, SAXException, ParserConfigurationException,
			XPathExpressionException, ParseException {
		String query = getQueryUrlForValidGreaterThanQueryContainsExpectedResources();
		validateNonEmptyResponse(query);
	}

	@Test
	public void validCompoundQueryContainsExpectedResource()
			throws IOException, SAXException, ParserConfigurationException,
			XPathExpressionException {
		String query = getQueryUrlForValidCompoundQueryContainsExpectedResources();
		validateNonEmptyResponse(query);
	}

	@Test
	public void fullTextSearchContainsExpectedResults() throws IOException,
			ParserConfigurationException, SAXException,
			XPathExpressionException {
		String query = getQueryUrlForFullTextSearchContainsExpectedResults();
		validateNonEmptyResponse(query);
	}
}